<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bd_alfredoramos' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'mysql' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'wB>R2:9 -HNOde)w YRJ}TL4g*.F4~f/>x_E5|uyN9mWM7#ZAtg1b9?4`AGOWo_M' );
define( 'SECURE_AUTH_KEY',  'a)~Ew,kGOV[{ 17_=W$5><9#euo_p^7jmt nxs]Q>LH]sca37?N8;6!)rcTs>x)V' );
define( 'LOGGED_IN_KEY',    'u)ag*ucC1Vzr3+)FtNS|#[Z@0tA6zyo[cT4nab{Ud8@vx{g8a|hLUujMkU<)DOqh' );
define( 'NONCE_KEY',        'J&bri[.%,@AMRqG/,B6D,rS1Z^fSPS;)1[20RTA4x|A019?8}HK)>r,2c0eZoA-G' );
define( 'AUTH_SALT',        'g*kmBBr[x~dJJpSVT&I8=n9-DLgohu:Vlx=seh8pij;VSBGuOi-f2TT^-mP&x!9n' );
define( 'SECURE_AUTH_SALT', 'qP)^Nj=aUAuUlQAkR1>7>=EDKtE `4NL]MU{yd^Z;B7,8?1%.umLb2/1mR)H[hT5' );
define( 'LOGGED_IN_SALT',   '37W3@U%4?$k#Lr_!XpB/ArxOA`u1f&LJ%/8.j!o~/H!(Zu0 >.oeI8oqg],UH~CU' );
define( 'NONCE_SALT',       '-l8d+M|][:{sLC`X*x@,1,G=.dJa0!ig{ML//|F 0K6?@Nc=tiuS!Nup{#;ST# R' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = '_ar_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
