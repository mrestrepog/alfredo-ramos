<?php get_header() ?>
<?php the_post() ?>
  <section id="main">
    <section id="banner-ppl" class="banner-ppl banner-bg" style="background: url('<?php echo get_template_directory_uri() ?>/library/images/backgrounds/banner-ppal.png') center no-repeat">
      <div class="content_banner">
        <h1 class="title_banner title-name">Alfredo<br><strong>Ramos</strong></h1>
        <article class="text-basic"><?php the_field('text-banner') ?></article>
        <a href="#"></a>
      </div>
      <figure class="arrow-banner">
        <img src="<?php echo get_template_directory_uri() ?>/library/images/icons/arrow-banner.svg" alt="Arrow banner">
      </figure>
    </section>
    <section id="events">
      <div class="container">
        <div class="search-content">
          <div class="row">
            <div class="col-lg-6 col-md-6">
              <div class="input_search">
                <label id="open_list-interested" for="#interested" class="text-subtitle text-subtitle_green">Estoy interesado en:</label>
                <div class="open-list open-interested arrow-down">Conocer a Alfredo</div>
                <ul id="list-interested" class="list-items">
                  <li class="item-input">Campo 1</li>
                  <li class="item-input">Campo 2</li>
                  <li class="item-input">Campo 3</li>
                  <li class="item-input">Campo 4</li>
                </ul>
              </div>
            </div>
            <div class="col-lg-6 col-md-6">
              <div class="input_search">
                <label for="#medellin" class="text-subtitle text-subtitle_green">Medellín adelante en:</label>
                <div id="open_list-medellin" class="open-list open-interested arrow-down">Movilidad</div>
                <ul id="list-medellin" class="list-items">
                  <li class="item-input">Campo 1</li>
                  <li class="item-input">Campo 2</li>
                  <li class="item-input">Campo 3</li>
                  <li class="item-input">Campo 4</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </section>
<?php get_footer() ?>