<!doctype html>
<!--[if lt IE 7]><html <?php language_attributes() ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes() ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes() ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes() ?> class="no-js"><!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php wp_title('') ?></title>

    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- 
    <meta name="msapplication-TileColor" content="#">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri() ?>/library/images/icons/favicon.ico">
    <meta name="theme-color" content="#"> 
    -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Montserrat:300,400,700&display=swap" rel="stylesheet">

    <?php wp_head() ?>
  </head>
  <body <?php body_class() ?>>
    <header class="header-main">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-4">
            <figure class="logo">
              <a href="#">
                <img src="<?php echo get_template_directory_uri() ?>/library/images/logo.svg" alt="Alfredo Ramos - Logo">
              </a>
            </figure>
          </div>
          <div class="col-lg-8 col-md-8">
            <nav class="nav-header">
              <?php wp_nav_menu(['container' => false, 'menu_class' => 'list-inline', 'menu-ppal' => __( 'The Main Menu', 'bonestheme' ), 'theme_location' => 'main-nav' ]); ?>
            </nav>
            <nav class="nav-header nav-redes-header">
              <ul class="list-inline">
                <li class="item-redes"><a href="#"><img src="<?php echo get_template_directory_uri() ?>/library/images/icons/icon-fb.svg" alt="Facebook - Ícono"></a></li>
                <li class="item-redes"><a href="#"><img src="<?php echo get_template_directory_uri() ?>/library/images/icons/icon-in.svg" alt="Twitter - Ícono"></a></li>
                <li class="item-redes"><a href="#"><img src="<?php echo get_template_directory_uri() ?>/library/images/icons/icon-ins.svg" alt="Instagram - Ícono"></a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </header>
